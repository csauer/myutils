#!/usr/bin/env python

try:
  import myutils
  print("[TEST] Congratulations, myutils is now installed on your system :).")
except ImportError, e:
  print("[WARNING] Module `myutils` is not installed on your system. Something went wrong.")
